module Download
  class Ftp < Base
    def connect(url, user: nil, password: nil)
      super(url)
      raise Error, 'url is invalid' unless (@url.scheme == 'ftp')
      @url.user = user if user
      @url.password = password if password
    end
  end
end