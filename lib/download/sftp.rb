require 'net/sftp'

module Download
  class Sftp < Base
    def connect(url, user, password)
      @url = URI(url)
      @user = user
      @password = password
    end

    def download
      Net::SFTP.start("#{@url.scheme}://#{@url.host}", @user, password: @password) do |sftp|
        begin
          sftp.download!(@url.path, file_name)
        rescue
          handle_exist_file
        end
      end
    end
  end
end