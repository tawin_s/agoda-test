module Download
  class Config
    # Controls which dir to write file
    # Default to '/'
    # @return [String]
    attr_accessor :root_dir

    def initialize
      @root_dir = ''
    end
  end

  # return [Download::Config] current config
  def self.config
    @config ||= Config.new
  end

  def self.reset
    @config = Config.new
  end

  # Set Download's Configuration
  # @param config Download::Config
  def self.config=(config)
    @config = config
  end

  def self.configure
    yield config
  end
end
