module Download
  class Http < Base

    def connect(url)
      super(url)
      raise Error, 'url is invalid' unless (@url.scheme == 'http' || @url.scheme == 'https')
    end
  end
end
