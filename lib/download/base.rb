require 'open-uri'
require 'net/http'

module Download
  class Base
    Error = Class.new(StandardError)

    DOWNLOAD_ERRORS = [
        SocketError,
        OpenURI::HTTPError,
        URI::InvalidURIError,
        RuntimeError
    ]

    def initialize;
    end


    # set url to current url
    # return nil
    # params url string
    def connect(url)
      @url = URI(URI.encode(URI.decode(url)))
    end

    # Set Download's Configuration
    # return Integer file size
    # @param file_name string
    def download
      raise(Error, 'Download.config.root_dir is not directory') unless File.directory?(File.join(Download.config.root_dir))
      write_file_to_disk(file_name)
    rescue *DOWNLOAD_ERRORS => error
      handle_exist_file
      raise if error.instance_of?(RuntimeError) && error.message !~ /redirection/
      raise Error, "download failed: #{error.message}"
    end

    protected def file_name
      File.join(Download.config.root_dir, File.basename(@url.path))
    end

    protected def handle_exist_file
      if File.exist?(file_name)
        File.delete(file_name)
      end
    end


    private def write_file_to_disk(file_name)
      File.open(file_name, 'w') do |f|
        IO.copy_stream(@url.open, f)
      end
    end
  end
end