# Download

## local Install

```sh
    $ bundle install #install dependency
    $ rake install #build and load gem to current env
    $ irb #To open ruby consule
```

```irb
    > require 'dowlonad'
```



## Usage
You can config a path that you want want to dowload by
```ruby
    Download.configure do |config|
        config.root_dir = '/'
    end
```

```ruby
    Download.config = '/'
```

You can download file by connect to server and download
Http
```ruby
    test = Download::Http.new
    test.connect('http://test.jpg')
    test.download
```

Ftp User and password is optional
```ruby 
    test = Download::Ftp.new
    # user and password is optional
    test.connect('ftp://test.jpg', user, password)
    test.download
```

Sftp User and password is optional, 
There is no spec for sftp because it is just a api wrapper for [net/sftp](https://github.com/net-ssh/net-sftp) gem
```ruby
    test = Download::Sftp.new
    # user and password is optional
    test.connect('sftp://test.jpg', user, password)
    test.download
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
 
## Test 
This gem use rspec to test 

