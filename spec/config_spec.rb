require 'spec_helper'

describe 'Config' do
  let!(:root_dir) {'/usr/tawin/base_image'}
  it 'set default root_dir to nil' do
    expect(Download.config.root_dir).to eq('')
  end

  it 'set configure block' do
    Download.configure do |config|
      config.root_dir = root_dir
    end
    expect(Download.config.root_dir).to equal(root_dir)
  end

  it 'get/set config root_path' do
    Download.config.root_dir = root_dir
    expect(Download.config.root_dir).to equal(root_dir)
  end
end
