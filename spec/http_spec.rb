require 'spec_helper'

describe 'Download::Http' do
  subject { Download::Http.new }
  let! (:image_url) { 'http://www.mrwallpaper.com/wallpapers/nice-girl.jpg' }
  let! (:open_image) { double('open_image') }
  let! (:file) { double('file') }

  describe '#connect' do
    let! (:uri) { double('uri') }

    it 'check valid url' do
      expect {
        subject.connect('bad_uri')
      }.to raise_error(Download::Http::Error, 'url is invalid')
    end

    it 'set instance variable' do
      subject.connect(image_url)
      expect(subject.instance_variable_get(:@url)).to eq(URI(URI.encode(URI.decode(image_url))))
    end
  end

  describe '#download' do
    let! (:file_name) { 'test.jpg' }
    let! (:uri) { URI(image_url) }
    let! (:file) { double('file') }
    let! (:file_path) { '/test' }

    let!(:full_file_path) { File.join(file_path, File.basename(uri.path)) }

    before do
      VCR.use_cassette('pretty_girl_image') do
        Download.config.root_dir = file_path

        allow(File).to receive(:directory?)
                           .and_return(file)
        allow(File).to receive(:open)
                           .with(File.join('/test', file_name), "w")

        subject.connect(image_url)
      end
    end

    context 'check root_dir config is directory' do
      before do
        Download.reset
        allow(File).to receive(:directory?).and_return(false)
      end

      it do
        VCR.use_cassette('pretty_girl_image') do
          expect {
            subject.download
          }.to raise_error(Download::Http::Error, 'Download.config.root_dir is not directory')
        end
      end
    end

    context 'save file to disk' do
      before do
        Download.config.root_dir = file_path
        allow(File).to receive(:open)
                           .with(full_file_path, "w")
        subject.connect(image_url)
      end

      it do
        VCR.use_cassette('pretty_girl_image') do
          subject.download
          expect(File).to have_received(:open).with(full_file_path, 'w')
        end
      end
    end

    it 'handle Delete existing file in error block' do
      allow(File).to receive(:exist?).and_return(true)
      allow_any_instance_of(Download::Http).to receive(:write_file_to_disk).and_raise(SocketError)
      expect {
        expect(File).to have_received(:delete)
      }
    end


    it 'handle socket error' do
      VCR.use_cassette('pretty_girl_image') do
        allow_any_instance_of(Download::Http).to receive(:write_file_to_disk).and_raise(SocketError)
        expect {
          subject.download
        }.to raise_error(Download::Http::Error, 'download failed: SocketError')
      end
    end

    it 'handle OpenURI::HTTPError error' do
      VCR.use_cassette('pretty_girl_image') do
        allow_any_instance_of(Download::Http).to receive(:write_file_to_disk).and_raise(OpenURI::HTTPError.new('', ''))
        expect {
          subject.download
        }.to raise_error(Download::Http::Error, 'download failed: ')
      end
    end

    it 'handle OpenURI::HTTPError error' do
      VCR.use_cassette('pretty_girl_image') do
        allow_any_instance_of(Download::Http).to receive(:write_file_to_disk).and_raise(OpenURI::HTTPError.new('', ''))
        expect {
          subject.download
        }.to raise_error(Download::Http::Error, 'download failed: ')
      end
    end

    it 'handle URI::InvalidURIError error' do
      VCR.use_cassette('pretty_girl_image') do
        allow_any_instance_of(Download::Http).to receive(:write_file_to_disk).and_raise(URI::InvalidURIError)
        expect {
          subject.download
        }.to raise_error(Download::Http::Error, 'download failed: URI::InvalidURIError')
      end
    end

    it 'handle runtime redirection error' do
      VCR.use_cassette('pretty_girl_image') do
        allow_any_instance_of(Download::Http).to receive(:write_file_to_disk).and_raise(RuntimeError, 'redirection')
        expect {
          subject.download
        }.to raise_error(Download::Http::Error, 'download failed: redirection')
      end
    end
  end
end
